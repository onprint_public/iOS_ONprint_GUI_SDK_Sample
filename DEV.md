# iOS ONprint GUI SDK Developer Guide

Welcome to the ONprint developer guide.<br/>

## Fundamentals

All the basic function calls (like SDK init) are detailed in [light SDK developer guide][1].

[1]: https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample/blob/master/DEV.md

## GUI functions

### Camera Controller

You can initialize the complete camera view controller from ```Onprint``` class

```
public static func rootViewController() -> UINavigationController
```

### Camera View

```CameraManager```class manages the camera view initialization and display settings.

To initialize the manager :

```
let cameraManager = CameraManager()
```

To display the camera view inside a view :

```
cameraManager.configureInView(view)
```

Available functions to handle the flash and to capture a picture:

```
public func toggleFlash() -> Bool
public func capturePicture(completion: @escaping (UIImage?, Error?) -> Void)
```
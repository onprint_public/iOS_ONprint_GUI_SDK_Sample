
# iOS ONprint GUI SDK Sample

In no time, create an application to get information of enriched images by taking picture.

## Introduction

This [Sample API][1] shows you how to use basic functionalities of ONprint GUI SDK.
By using [ONprint API][2] and [ONprint Light SDK][3], it provides you an interface for individual camera
connected to an iOS device in order
to get informations from picture.

GUI will use ONprint interface design used especially in [ONprint Application][4].
So when you flash an enriched image, associated image actions will poped up!
A flash historic is also available.

## Requirements

- iOS 9.0+
- Xcode 10+
- Swift 4.2+


## Screenshots

<img src="screenshots/main.jpg" height="400" alt="Screenshot"/>


## SDK Installation

To start, install the SDK for your own project by following the [install doc][5].
<br/><br/>
Then, to integrate ONprint functions, please consult the [developer guide][6].


### Support

If you've found an error in this sample or if you have any question,
please send us an email to: mobile@onprint.com


### License

© 2018 by LTU Tech. All Rights Reserved.



[1]: https://gitlab.com/onprint_public/iOS_ONprint_GUI_SDK_Sample
[2]: http://developer.onprint.com/api.html
[3]: https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample
[4]: https://itunes.apple.com/fr/app/onprint-limprim%C3%A9-connect%C3%A9/id609688732?mt=8
[5]: https://gitlab.com/onprint_public/iOS_ONprint_GUI_SDK_Sample/blob/master/INSTALL.md
[6]: https://gitlab.com/onprint_public/iOS_ONprint_GUI_SDK_Sample/blob/master/DEV.md

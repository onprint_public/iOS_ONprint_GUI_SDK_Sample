# Installation


The following will guide you through the ONprintSDK installation process.

## Add the framework

To integrate ONprintSDK into your Xcode project you need to :

* download the latest version of ["OnprintSDK.framework"][1]
* in the Project Navigator, click on your project and drag the OnprintSDK.framework into the "Embedded Binaries" section of the application target

<img src="screenshots/embedded_binaries.png" height="400" alt="EmbeddedBinaries"/>

## Permissions

If you're project is iOS 10+ you have to define and provide a usage description of the following system’s privacy-sensitive data accessed in your ```Info.plist``` file:

* Privacy — Calendars Usage Description 
* Privacy — Camera Usage Description
* Privacy — Contacts Usage Description


## Initialize the SDK

Inside your project ```AppDelegate```, add in the upper part:

```
import OnprintSDK
``` 

Use your API Key to initialize the SDK:

```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Onprint.setApiKey("INSERT YOUR API KEY HERE")
        return true
    }
```

The API Key is a character chain allowing you to use ONprint API services with your dedicating data. To obtain it, please contact mobile@onprint.com

## Launch camera

You can initialize and set the application root controller in your ```AppDelegate```:

```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Onprint.setApiKey("INSERT YOUR API KEY HERE")
        self.window?.rootViewController = Onprint.rootViewController()
        return true
    }
```


## Usage

Now it's time... to code!
Please follow -> the [developer guide][2]


[1]: http://developer.onprint.com/sdkbinaries/ios/SDK/GUI/ONprintSDK-ios-3.0.7.zip
[2]: https://gitlab.com/onprint_public/iOS_ONprint_GUI_SDK_Sample/blob/master/DEV.md
